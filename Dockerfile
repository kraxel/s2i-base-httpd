FROM quay.io/almalinuxorg/8-base

ENV SUMMARY="httpd base" \
    DESCRIPTION="base image for httpd s2i images"

LABEL maintainer="Gerd Hoffmann <kraxel@redhat.com>" \
      summary="${SUMMARY}" \
      description="${DESCRIPTION}" \
      io.k8s.display-name="${SUMMARY}" \
      io.k8s.description="${DESCRIPTION}" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="httpd" \
      io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

USER root

COPY ./etc/centos-mirror.sh /root
COPY ./etc/proxy.sh /etc/profile.d
COPY ./etc/*.conf /etc/httpd/conf.d/
COPY ./s2i/bin/ /usr/libexec/s2i

RUN /root/centos-mirror.sh; \
    source /etc/profile.d/proxy.sh; \
    dnf update -y && \
    dnf install -y httpd && \
    dnf clean all -y
RUN mkdir -p /run/httpd;\
    chmod 777 /run/httpd /etc/httpd/logs;\
    sed -i -e '/Listen/s/^/#/' /etc/httpd/conf/httpd.conf;\
    rm -f /etc/httpd/conf.d/welcome.conf
RUN mkdir -p /opt/app-root;\
    chmod 755 /opt /opt/app-root;\
    useradd -d /opt/app-root/src -u 1001 default

USER 1001
EXPOSE 8080

CMD ["/usr/libexec/s2i/usage"]
