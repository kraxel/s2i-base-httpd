#!/bin/sh

#
# use fixed mirror (be cache proxy friendly)
#
furl="http://mirror.centos.org/"
turl="${CENTOS_MIRROR-http://mirror.centos.org/}"
for file in /etc/yum.repos.d/CentOS*.repo; do
	sed	-i.orig \
		-e 's/^mirrorlist/#mirrorlist/' \
		-e 's/^#baseurl/baseurl/' \
		-e "s|${furl}|${turl}|" \
		"$file"
done

#
# no parallel downloads please
#
grep -q max_parallel_downloads /etc/dnf/dnf.conf ||\
echo max_parallel_downloads=1 >> /etc/dnf/dnf.conf
